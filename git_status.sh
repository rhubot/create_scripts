#!/bin/bash -i

# Get color definitions and other scripts
if [ ! -d ${INSTALL_SCRIPTS_DIR}/ ]
then
    git clone https://bitbucket.org/rhubot/install_scripts.git ~/install_scripts
    source ~/install_scripts/configure_install_scripts.sh
    
    set +e
    trap - ERR
    source ~/.bashrc
    set -e
    trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR
    
fi
source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

# Remember where we started
SCRIPTS_DIR=$(pwd)

false=0
true=1

givenDirIsRepo=${true}

# Default search directory
SEARCH_DIR=".."

# Get the Git status of a given directory and display text corresponding to that status
function display_status () {

    git_status=$(git status 2>&1)
    
    if [[ ${git_status} == *"Changes not staged for commit"* ]]
    then
        echo -e "${RED}--> Commits needed in $(pwd)${NC}"
    elif [[ ${git_status} == *"Your branch is ahead"* ]]
    then
        echo -e "${RED}--> Push needed in $(pwd)${NC}"
    elif [[ ${git_status} == *"Untracked files"* ]]
    then
        echo -e "${YELLOW}--> Untracked files in $(pwd)${NC}"
    elif [[ ${git_status} == *"fatal: Not a git repository"* ]] || [[ ${git_status} == *"fatal: not a git repository"* ]]
    then
        #echo -e "${BLUE}    Not a git repo: $(pwd)${NC}"
        givenDirIsRepo=${false}
    elif [[ ${git_status} == *"nothing to commit, working directory clean"* ]] || [[ ${git_status} == *"nothing to commit, working tree clean"* ]]
    then
        echo -e "${LIME}    Nothing to commit in $(pwd)${NC}"
    else
        echo -e "${MAROON}    Unknown state of $(pwd): ${git_status}${NC}"
    fi

}


# If we are given a directory to search, use that directory instead
if test $# -eq 1
then
    if [ "${1}" == "--ros" ]
    then
        SEARCH_DIR="../catkin_ws/src/"
    else
        SEARCH_DIR=${1}
    fi
    
    # Do the search
    cd ${SEARCH_DIR}
    echo ""
    display_status ${SEARCH_DIR}
    
    # ... and sub-directories (1 level deep) if the given directory was not a repo
    if [ ${givenDirIsRepo} -eq ${false} ]
    then
        for d in ./*/
        do
            # if the directory ${d} exists...
            if [ -d ${d} ]
            then
                cd ${d}
                display_status ${d}
                cd ..
            fi
        done
    fi
    echo ""
elif test $# -eq 0
then
    source ./git_status.sh ~
    
    if [[ -d ~/catkin_ws/src/ ]]
    then
        source ./git_status.sh --ros
    fi
fi


# Return to the original directory
cd ${SCRIPTS_DIR}
