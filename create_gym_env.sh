#!/bin/bash -i

# Remember where we started
SCRIPTS_DIR=$(pwd)

# Get color definitions and other scripts
if [ ! -d ${INSTALL_SCRIPTS_DIR}/ ]
then
    git clone https://bitbucket.org/rhubot/install_scripts.git ~/install_scripts
    source ~/install_scripts/configure_install_scripts.sh
    
    set +e
    trap - ERR
    source ~/.bashrc
    set -e
    trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR
    
fi
source ${INSTALL_SCRIPTS_DIR}/colors.sh ""


# Check for correct args
if [ "$#" = 0 ] || [ "${1}" = "-h" ] || [ "${1}" = "--help" ]
then
    echo -e "${WARNING_COLOR}    Usage: create_gym_env.sh <environment-name>${NC}" >&2
    echo -e "${WARNING_COLOR}    Example: create_gym_env.sh tic_tac_toe${NC}" >&2
    exit 1
fi

# Create the workspace
if [ ! -d ~/catkin_ws/src/ ]
then
    # Create a catkin workspace if it does not exist
    source ${CREATE_SCRIPTS_DIR}/create_catkin_ws.sh
fi
cd ~/catkin_ws/src/


env_name=${1}                # e.g. tic_tac_toe
common_name=""               # e.g. Tic Tac Toe
registered_env_name=""       # e.g. TicTacToe-v0
class_name=""                # e.g. TicTacToe


# Figure out the common name
search_str="_"

# Get the index of search_str in env_name
index=`expr index "${env_name}" "${search_str}"`

# Initialize some variables for later
common_name=""
remaining_str="${env_name}"

# Keep looping while we still are able to find search_str in env_name
while [ ${index} -ne 0 ]
do
    # Get the first half of the string (between the first letter of remaining_str and index-1)
    first_half_str=${remaining_str:0:index-1}
    
    # Get the second half of the string (between the index and the end of the remaining_str)
    remaining_str=${remaining_str:index}
    
    # Convert the first letter of first_half_str into a capital letter and then add it to common_name
    common_name="${common_name} ${first_half_str^}"
    
    # Get the index of search_str in remaining_str
    index=`expr index "${remaining_str}" "${search_str}"`
    
    # If we were not able to find the search_str...
    if [ "$index" -eq "0" ]
    then
        # Convert the first letter of remaining_str into a capital letter and then add it to common_name
        common_name="${common_name} ${remaining_str^}"
    fi
done


# Figure out the registered name
search_str="_"

# Get the index of search_str in env_name
index=`expr index "${env_name}" "${search_str}"`

# Initialize some variables for later
registered_env_name=""
remaining_str="${env_name}"

# Keep looping while we still are able to find search_str in env_name
while [ ${index} -ne 0 ]
do
    # Get the first half of the string (between the first letter of remaining_str and index-1)
    first_half_str=${remaining_str:0:index-1}
    
    # Get the second half of the string (between the index and the end of the remaining_str)
    remaining_str=${remaining_str:index}
    
    # Convert the first letter of first_half_str into a capital letter and then add it to registered_env_name
    registered_env_name="${registered_env_name}${first_half_str^}"
    
    # Get the index of search_str in remaining_str
    index=`expr index "${remaining_str}" "${search_str}"`
    
    # If we were not able to find the search_str...
    if [ "$index" -eq "0" ]
    then
        # Convert the first letter of remaining_str into a capital letter and then add it to registered_env_name
        class_name="${registered_env_name}${remaining_str^}"
        registered_env_name="${registered_env_name}${remaining_str^}-v0"
    fi
done


folder_name="${env_name}_env"
setup_script_name="setup_${env_name}.sh"
update_script_name="update_${env_name}.sh"
test_script_name="test_${env_name}.py"
init_script_name="__init__.py"
environment_script_name="${env_name}.py"

if [ "$#" = 0 ] || [ "${1}" = "-h" ] || [ "${1}" = "--help" ]
then
    echo -e "${ERROR_COLOR}The folder ${folder_name} already exist. Aborting creation of ${env_name}${NC}"
    exit 1
fi

#echo "Using env_name: ${env_name}"
#echo "Using common_name: ${common_name}"
#echo "Using registered_env_name: ${registered_env_name}"
#echo "Using class_name: ${class_name}"
#echo "Using setup_script_name: ${setup_script_name}"
#echo "Using update_script_name: ${update_script_name}"
#echo "Using test_script_name: ${test_script_name}"
#echo "Using init_script_name: ${init_script_name}"
#echo "Using environment_script_name: ${environment_script_name}"


# Create the environment
echo -e "${TITLE_COLOR}Creating Gym Environment: ${env_name}${NC}"

mkdir ${folder_name}
cd ${folder_name}


# Setup Script
echo "#!/bin/bash -i                                                                                                       " >> ${setup_script_name}
echo "                                                                                                                     " >> ${setup_script_name}
echo "# Get color definitions and other scripts                                                                            " >> ${setup_script_name}
echo "if [ ! -d \${INSTALL_SCRIPTS_DIR}/ ]                                                                                 " >> ${setup_script_name}
echo "then                                                                                                                 " >> ${setup_script_name}
echo "    cd ~                                                                                                             " >> ${setup_script_name}
echo "    git clone https://bitbucket.org/rhubot/install_scripts.git                                                       " >> ${setup_script_name}
echo "fi                                                                                                                   " >> ${setup_script_name}
echo "source \${INSTALL_SCRIPTS_DIR}/colors.sh                                                                             " >> ${setup_script_name}
echo "                                                                                                                     " >> ${setup_script_name}
echo "                                                                                                                     " >> ${setup_script_name}
echo "# Copy the env to the proper location                                                                                " >> ${setup_script_name}
echo "source ./${update_script_name}                                                                                       " >> ${setup_script_name}
echo "                                                                                                                     " >> ${setup_script_name}
echo "# Register the env with Gym                                                                                          " >> ${setup_script_name}
echo "echo -e \"\${TITLE_COLOR}Registering ${common_name} with Gym\${NC}\"                                                 " >> ${setup_script_name}
echo "                                                                                                                     " >> ${setup_script_name}
echo "if ! grep -q \"${registered_env_name}\" ~/gym/gym/envs/__init__.py                                                   " >> ${setup_script_name}
echo "then                                                                                                                 " >> ${setup_script_name}
echo "                                                                                                                     " >> ${setup_script_name}
echo "    echo \" \"                                                                     >> ~/gym/gym/envs/__init__.py     " >> ${setup_script_name}
echo "    echo \"# ${common_name}\"                                                      >> ~/gym/gym/envs/__init__.py     " >> ${setup_script_name}
echo "    echo \"# ------------\"                                                        >> ~/gym/gym/envs/__init__.py     " >> ${setup_script_name}
echo "    echo \" \"                                                                     >> ~/gym/gym/envs/__init__.py     " >> ${setup_script_name}
echo "    echo \"register(\"                                                             >> ~/gym/gym/envs/__init__.py     " >> ${setup_script_name}
echo "    echo \"    id='${registered_env_name}',\"                                      >> ~/gym/gym/envs/__init__.py     " >> ${setup_script_name}
echo "    echo \"    entry_point='gym.envs.${env_name}:${class_name}',\"                 >> ~/gym/gym/envs/__init__.py     " >> ${setup_script_name}
echo "    echo \")\"                                                                     >> ~/gym/gym/envs/__init__.py     " >> ${setup_script_name}
echo "    echo \" \"                                                                     >> ~/gym/gym/envs/__init__.py     " >> ${setup_script_name}
echo "                                                                                                                     " >> ${setup_script_name}
echo "    echo -e \"\${FINISHED_TITLE_COLOR}Done\${NC}\"                                                                   " >> ${setup_script_name}
echo "else                                                                                                                 " >> ${setup_script_name}
echo "    echo -e \"\${WARNING_COLOR}${common_name} already registered\${NC}\"                                             " >> ${setup_script_name}
echo "fi                                                                                                                   " >> ${setup_script_name}
chmod 755 ./${setup_script_name}


# Update Script
echo "#!/bin/bash -i                                                                                                       " >> ${update_script_name}
echo "                                                                                                                     " >> ${update_script_name}
echo "# Get color definitions and other scripts                                                                            " >> ${update_script_name}
echo "if [ ! -d \${INSTALL_SCRIPTS_DIR}/ ]                                                                                 " >> ${update_script_name}
echo "then                                                                                                                 " >> ${update_script_name}
echo "    cd ~                                                                                                             " >> ${update_script_name}
echo "    git clone https://bitbucket.org/rhubot/install_scripts.git                                                       " >> ${update_script_name}
echo "fi                                                                                                                   " >> ${update_script_name}
echo "source \${INSTALL_SCRIPTS_DIR}/colors.sh                                                                             " >> ${update_script_name}
echo "                                                                                                                     " >> ${update_script_name}
echo "                                                                                                                     " >> ${update_script_name}
echo "gym_env=\"~/gym/gym/envs/\"                                                                                          " >> ${update_script_name}
echo "                                                                                                                     " >> ${update_script_name}
echo "echo -e \"\${TITLE_COLOR}Copying ${env_name} environment to \${gym_env}\${NC}\"                                      " >> ${update_script_name}
echo "cp -r ./${env_name}/ ~/gym/gym/envs/                                                                                 " >> ${update_script_name}
echo "echo -e \"\${FINISHED_TITLE_COLOR}Done\${NC}\"                                                                       " >> ${update_script_name}
chmod 755 ./${update_script_name}


# Test script
echo "#!/usr/bin/env python                                                                                                " >> ${test_script_name}
echo "                                                                                                                     " >> ${test_script_name}
echo "import gym                                                                                                           " >> ${test_script_name}
echo "                                                                                                                     " >> ${test_script_name}
echo "                                                                                                                     " >> ${test_script_name}
echo "env = gym.make(\"${registered_env_name}\") # Create ${class_name} environment                                        " >> ${test_script_name}
echo "                                                                                                                     " >> ${test_script_name}
echo "print \"Loaded ${registered_env_name}\"                                                                              " >> ${test_script_name}
echo "print \"Observation Space: \", (env.observation_space)                                                               " >> ${test_script_name}
echo "print \"Action Space: \", env.action_space                                                                           " >> ${test_script_name}
echo "try:                                                                                                                 " >> ${test_script_name}
echo "    # Discrete spaces only...                                                                                        " >> ${test_script_name}
echo "    print \"Action Description: \", env.unwrapped.get_action_meanings()                                              " >> ${test_script_name}
echo "except AttributeError:                                                                                               " >> ${test_script_name}
echo "    pass                                                                                                             " >> ${test_script_name}
echo "                                                                                                                     " >> ${test_script_name}
echo "print \"Running tests...\"                                                                                           " >> ${test_script_name}
echo "                                                                                                                     " >> ${test_script_name}
echo "numSteps = 10                                                                                                        " >> ${test_script_name}
echo "numEpisodes = 5                                                                                                      " >> ${test_script_name}
echo "                                                                                                                     " >> ${test_script_name}
echo "for i_episode in range(numEpisodes):                                                                                 " >> ${test_script_name}
echo "  observation = env.reset()                                                                                          " >> ${test_script_name}
echo "  print \"        Initial Observations: \"                                                                           " >> ${test_script_name}
echo "  print \"            Observation[0]: \", observation[0]                                                             " >> ${test_script_name}
echo "  print \"            Observation[1]: \", observation[1]                                                             " >> ${test_script_name}
echo "                                                                                                                     " >> ${test_script_name}
echo "  for i in range(numSteps): # Run for N steps                                                                        " >> ${test_script_name}
echo "    env.render()                                                                                                     " >> ${test_script_name}
echo "    action = env.action_space.sample()                                                                               " >> ${test_script_name}
echo "    observation, reward, done, info = env.step(action)                                                               " >> ${test_script_name}
echo "    print \"        Observations: \"                                                                                 " >> ${test_script_name}
echo "    print \"            \", info[\"observation_space_0\"], \": \", observation[0]                                    " >> ${test_script_name}
echo "    print \"            \", info[\"observation_space_1\"], \": \", observation[1]                                    " >> ${test_script_name}
echo "                                                                                                                     " >> ${test_script_name}
echo "    if done:                                                                                                         " >> ${test_script_name}
echo "      print \"---------------------------------------------------\"                                                  " >> ${test_script_name}
echo "      print \"Episode finished after {} timesteps\".format(i+1)                                                      " >> ${test_script_name}
echo "      print \"Final Observations: \"                                                                                 " >> ${test_script_name}
echo "      print \"    \", info[\"observation_space_0\"], \": \", observation[0]                                          " >> ${test_script_name}
echo "      print \"    \", info[\"observation_space_1\"], \": \", observation[1]                                          " >> ${test_script_name}
echo "      print \"Completed episode \", (i_episode+1), \" of \", numEpisodes                                             " >> ${test_script_name}
echo "      print \"===================================================\"                                                  " >> ${test_script_name}
echo "      break                                                                                                          " >> ${test_script_name}
echo "env.close()                                                                                                          " >> ${test_script_name}


mkdir ${env_name}
cd ./${env_name}


# Init file
echo "from gym.envs.${env_name}.${env_name} import ${class_name}                                                           " >> ${init_script_name}


# Environment file
echo "import numpy as np                                                                                                   " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "import gym                                                                                                           " >> ${environment_script_name}
echo "from gym import error, spaces, utils                                                                                 " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "class ${class_name}(gym.Env):                                                                                        " >> ${environment_script_name}
echo "    metadata = {'render.modes': ['human']}                                                                           " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "    def __init__(self):                                                                                              " >> ${environment_script_name}
echo "        print(\"[ENV] Init\")                                                                                        " >> ${environment_script_name}
echo "        self.__version__ = \"0.0.1\"                                                                                 " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "        # Modify the observation space, low, high and shape values according to your custom environment's needs      " >> ${environment_script_name}
echo "        self.observation_space = gym.spaces.Box(low=0.0, high=1.0, shape=(3,))                                       " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "        # Modify the action space, and dimension according to your custom environment's needs                        " >> ${environment_script_name}
echo "        self.action_space = gym.spaces.Discrete(4)                                                                   " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "        self.info = default_dictionary                                                                               " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "        self.done = False                                                                                            " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "    def step(self, action):                                                                                          " >> ${environment_script_name}
echo "        print(\"[ENV] Step\")                                                                                        " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "        reward = self.calculateReward()                                                                              " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "        reportDone = self.done                                                                                       " >> ${environment_script_name}
echo "        self.done = not self.done                                                                                    " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "        position, velocity = self.state                                                                              " >> ${environment_script_name}
echo "        velocity += 0.1                                                                                              " >> ${environment_script_name}
echo "        position += velocity                                                                                         " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "        # REMEMBER: observation may not be the entire state... they can be two different things                      " >> ${environment_script_name}
echo "        self.state = (position, velocity)                                                                            " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "        return np.array(self.state), reward, reportDone, self.info                                                   " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "    def reset(self):                                                                                                 " >> ${environment_script_name}
echo "        print(\"[ENV] Reset\")                                                                                       " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "        self.done = False                                                                                            " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "        self.state = np.array([2, 0])                                                                                " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "        return np.array(self.state)                                                                                  " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "    def render(self, mode='human', close=False):                                                                     " >> ${environment_script_name}
echo "        print(\"[ENV] Render\")                                                                                      " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "    def calculateReward(self):                                                                                       " >> ${environment_script_name}
echo "        reward = 1.0                                                                                                 " >> ${environment_script_name}
echo "        return reward                                                                                                " >> ${environment_script_name}
echo "                                                                                                                     " >> ${environment_script_name}
echo "default_dictionary = {                                                                                               " >> ${environment_script_name}
echo "    \"observation_space_0\": \"location\",                                                                           " >> ${environment_script_name}
echo "    \"observation_space_1\": \"velocity\",                                                                           " >> ${environment_script_name}
echo "    \"action_space_0\": 1234                                                                                         " >> ${environment_script_name}
echo "}                                                                                                                    " >> ${environment_script_name}


echo -e "${FINISHED_TITLE_COLOR}Finished creating ${env_name}${NC}"


# Return to where we came from
cd ${SCRIPTS_DIR}
