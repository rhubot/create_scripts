#!/bin/bash -i

cd ~/catkin_ws

echo -n "Building source code..."

# Run the "catkin --version" command and funnel any error messages to stdout so they don't appear in the terminal
catkin_installed=$(catkin --version 2>&1)

# If the resulting text includes "catkin_tools" it means we can use the catkin command to build the code
if [[ "${catkin_installed}" == *"catkin_tools"* ]]
then
    build_output=$(catkin build 2>&1)
    if [[ "${build_output}" == *"was previously built by 'catkin_make'"* ]]
    then
        catkin_make
    fi
    if [[ "${build_output}" != *"[build]   Failed:    None"* ]]
    then
        echo ""
        echo "${build_output}"
    fi
else
    # The catkin command is not installed, use catkin_make (included with ROS install)
    catkin_make
fi

echo "Done!"
