#!/bin/bash -i

# Remember where we started
SCRIPTS_DIR=$(pwd)

# Get color definitions and other scripts
if [ ! -d ${INSTALL_SCRIPTS_DIR}/ ]
then
    git clone https://bitbucket.org/rhubot/install_scripts.git ~/install_scripts
    source ~/install_scripts/configure_install_scripts.sh
    
    set +e
    trap - ERR
    source ~/.bashrc
    set -e
    trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR
    
fi
source ${INSTALL_SCRIPTS_DIR}/colors.sh ""


# Check for correct args
if [ "$#" -ne 1 ] || [ "${1}" = "-h" ] || [ "${1}" = "--help" ]
then
    echo -e "${WARNING_COLOR}    Usage: make_into_git_repo.sh <path_to_local_folder>${NC}" >&2
    exit 1
fi

filePath=${1}

echo -e "${PROMPT_COLOR}"
read -p "Username: " username
read -s -p "Password: " password
echo -e "${NC}"

#Set repo name to the name of the directory
repo="${filePath%"${filePath##*[!/]}"}"
repo=${repo##*/}

# Turn the given directory into a local git repo
cd ${filePath}
echo -e "${TITLE_COLOR}Initializing local repo (${repo})${NC}"
git init
echo -e "${FINISHED_TITLE_COLOR}Finished initializing local repo${NC}"

# Tell Bitbucket to make a new repo
echo -e "${TITLE_COLOR}Initializing remote repo${NC}"
curl -X POST -v -H "Content-Type: application/json" -u ${username}:${password} -d '{"scm": "git", "is_private": true }' "https://api.bitbucket.org/2.0/repositories/rhubot/${repo}"

# Give the server 3 seconds to do its thing
sleep 3s
echo -e "${FINISHED_TITLE_COLOR}Finished initializing remote repo${NC}"

# Connect the local repo and remote repo
echo -e "${TITLE_COLOR}Configuring repo${NC}"
git remote add origin git@bitbucket.org:$username/$repo.git
git remote set-url origin https://${username}@bitbucket.org/${username}/${repo}.git

touch README.md
git add *
git commit -m "Initial commit"
git push -u origin master
echo -e "${FINISHED_TITLE_COLOR}Finished configuring repo${NC}"


# Return to where we came from
cd ${SCRIPTS_DIR}
