#!/bin/bash -i

# Remember where we started
SCRIPTS_DIR=$(pwd)

# Get color definitions and other scripts
if [ ! -d ${INSTALL_SCRIPTS_DIR}/ ]
then
    git clone https://bitbucket.org/rhubot/install_scripts.git ~/install_scripts
    source ~/install_scripts/configure_install_scripts.sh
    
    set +e
    trap - ERR
    source ~/.bashrc
    set -e
    trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR
    
fi
source ${INSTALL_SCRIPTS_DIR}/colors.sh ""


# Check for correct args
if [ "$#" = 0 ] || [ "${1}" = "-h" ] || [ "${1}" = "--help" ]
then
    echo -e "${WARNING_COLOR}    Usage: create_gym_env_in_repo.sh <environment-name>${NC}" >&2
    echo -e "${WARNING_COLOR}    Example: create_gym_env_in_repo.sh tic_tac_toe${NC}" >&2
    exit 1
fi


scriptsPath=$(pwd)
env_name=${1}
folder_name="${env_name}_env"

source ${scriptsPath}/create_gym_env.sh $@

source ${scriptsPath}/make_into_git_repo.sh ~/catkin_ws/src/${folder_name}


# Return to where we came from
cd ${SCRIPTS_DIR}
