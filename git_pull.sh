#!/bin/bash -i

# Get color definitions and other scripts
if [ ! -d ${INSTALL_SCRIPTS_DIR}/ ]
then
    git clone https://bitbucket.org/rhubot/install_scripts.git ~/install_scripts
    source ~/install_scripts/configure_install_scripts.sh
    
    set +e
    trap - ERR
    source ~/.bashrc
    set -e
    trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR
    
fi
source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

# Remember where we started
SCRIPTS_DIR=$(pwd)

false=0
true=1

givenDirIsRepo=${true}

# Default search directory
SEARCH_DIR=".."


# Get the Git status of a given directory and display text corresponding to that status
function performing_pull () {

    git_status=$(git status 2>&1)
    
    if [[ ${git_status} == *"Changes not staged for commit"* ]]
    then
        givenDirIsRepo=${true}
    elif [[ ${git_status} == *"Your branch is ahead"* ]]
    then
        givenDirIsRepo=${true}
    elif [[ ${git_status} == *"Untracked files"* ]]
    then
        givenDirIsRepo=${true}
    elif [[ ${git_status} == *"fatal: Not a git repository"* ]] || [[ ${git_status} == *"fatal: not a git repository"* ]]
    then
        #echo -e "${BLUE}    Not a git repo: ${1}${NC}"
        givenDirIsRepo=${false}
    elif [[ ${git_status} == *"nothing to commit, working directory clean"* ]] || [[ ${git_status} == *"nothing to commit, working tree clean"* ]]
    then
        givenDirIsRepo=${true}
    else
        echo -e "${MAROON}    Unknown state of ${1}: ${git_status}${NC}"
    fi
    
    if [ ${givenDirIsRepo} -eq ${true} ]
    then
        echo -e "    ${TITLE_COLOR}Pulling from $(pwd)${NC}"
        git_pull=$(git pull 2>&1)
        echo -e "    ${FINISHED_TITLE_COLOR}Done${NC}"
    fi
}


# If we are given a directory to search, use that directory instead
if test $# -eq 1
then
    if [ "${1}" == "--ros" ]
    then
        SEARCH_DIR="../catkin_ws/src/"
    else
        SEARCH_DIR=${1}
    fi
    
    # Do the search
    cd ${SEARCH_DIR}
    echo ""
    performing_pull ${SEARCH_DIR}

    # ... and sub-directories (1 level deep) if the given directory was not a repo
    if [ ${givenDirIsRepo} -eq ${false} ]
    then
        for d in ./*/
        do
            cd ${d}
            performing_pull ${d}
            cd ..
        done
    fi
    echo ""
elif test $# -eq 0
then
    source ./git_pull.sh ~
    
    if [[ -d ~/catkin_ws/src/ ]]
    then
        source ./git_pull.sh --ros
    fi
fi

# Return to the original directory
cd ${SCRIPTS_DIR}
