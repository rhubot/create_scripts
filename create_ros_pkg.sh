#!/bin/bash -i

# Remember where we started
SCRIPTS_DIR=$(pwd)

# Get color definitions and other scripts
if [ ! -d ${INSTALL_SCRIPTS_DIR}/ ]
then
    git clone https://bitbucket.org/rhubot/install_scripts.git ~/install_scripts
    source ~/install_scripts/configure_install_scripts.sh
    
    set +e
    trap - ERR
    source ~/.bashrc
    set -e
    trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR
    
fi
source ${INSTALL_SCRIPTS_DIR}/colors.sh ""


# Check for correct args
if [ "$#" = 0 ] || [ "${1}" = "-h" ] || [ "${1}" = "--help" ]
then
    echo -e "${WARNING_COLOR}    Usage: create_ros_pkg.sh <package-name> [dependencies...]${NC}" >&2
    exit 1
fi

# TODO: If no args are given, display a menu of all known ROS packages

# Using the command:  complete -p roscd
# We get the result:  complete -o nospace -F _roscomplete_sub_dir roscd
# The value after the -F flag (aka "_roscomplete_sub_dir") gives us the function that is called when attempting tab completion with the roscd command
# We can look at that function by calling:  type _roscomplete_sub_dir
# Within the function, we are looking for a line that includes the command "compgen"
# More specifically, we want the -w argument, which we see is the variable "opts"
# Earlier in the function, we see opts is set by calling the _ros_list_locations function
# We can call the _ros_list_locations function to get the list of ROS packages that the system knows about, including any ones created by the user after sourcing ~/catkin_ws/devel/setup.bash
#rosPkgs=$(_ros_list_locations)

#echo -e "ROS Packages:"
#echo -e "${rosPkgs}"

default_hz=30

function configure_for_cpp () {
    echo -e "${TITLE_COLOR}Configuring as CPP package${NC}"
    
    cmake_file_name="CMakeLists.txt"
    src_main_file_name="${package_name}_node.cpp"
    src_class_file_name="${package_name}.cpp"
    h_file_name="${package_name}.h"
    header_guard_name="${package_name^^}_H"
    variable_name=${class_name,}
    
    cd src
    
    # Create the main src file
    echo "#include \"ros/ros.h\"                                " >> ${src_main_file_name}
    echo "#include \"${package_name}/${h_file_name}\"           " >> ${src_main_file_name}
    echo "                                                      " >> ${src_main_file_name}
    echo "/**                                                   " >> ${src_main_file_name}
    echo " *                                                    " >> ${src_main_file_name}
    echo " */                                                   " >> ${src_main_file_name}
    echo "int main(int argc, char **argv)                       " >> ${src_main_file_name}
    echo "{                                                     " >> ${src_main_file_name}
    echo "    ros::init(argc, argv, \"${package_name}_node\");  " >> ${src_main_file_name}
    echo "                                                      " >> ${src_main_file_name}
    echo "    ros::NodeHandle n;                                " >> ${src_main_file_name}
    echo "    ros::Rate loop_rate(${default_hz});  // Hz        " >> ${src_main_file_name}
    echo "                                                      " >> ${src_main_file_name}
    echo "    ${class_name} ${variable_name};                   " >> ${src_main_file_name}
    echo "                                                      " >> ${src_main_file_name}
    echo "    while (ros::ok())                                 " >> ${src_main_file_name}
    echo "    {                                                 " >> ${src_main_file_name}
    echo "        ros::spinOnce();                              " >> ${src_main_file_name}
    echo "                                                      " >> ${src_main_file_name}
    echo "        loop_rate.sleep();                            " >> ${src_main_file_name}
    echo "    }                                                 " >> ${src_main_file_name}
    echo "                                                      " >> ${src_main_file_name}
    echo "                                                      " >> ${src_main_file_name}
    echo "    return 0;                                         " >> ${src_main_file_name}
    echo "}                                                     " >> ${src_main_file_name}
    
    
    # Create the class src file
    echo "#include \"ros/ros.h\"                                " >> ${src_class_file_name}
    echo "#include \"${package_name}/${h_file_name}\"           " >> ${src_class_file_name}
    echo "                                                      " >> ${src_class_file_name}
    echo "${class_name}::${class_name}()                        " >> ${src_class_file_name}
    echo "{                                                     " >> ${src_class_file_name}
    echo "    ROS_INFO(\"Created ${class_name} object\");       " >> ${src_class_file_name}
    echo "}                                                     " >> ${src_class_file_name}
    

    # Create the class header file
    cd ../include/${package_name}
    echo "#ifndef ${header_guard_name}                          " >> ${h_file_name}
    echo "#define ${header_guard_name}                          " >> ${h_file_name}
    echo "                                                      " >> ${h_file_name}
    echo "class ${class_name}                                   " >> ${h_file_name}
    echo "{                                                     " >> ${h_file_name}
    echo "private:                                              " >> ${h_file_name}
    echo "                                                      " >> ${h_file_name}
    echo "public:                                               " >> ${h_file_name}
    echo "    ${class_name}();                                  " >> ${h_file_name}
    echo "                                                      " >> ${h_file_name}
    echo "};                                                    " >> ${h_file_name}
    echo "                                                      " >> ${h_file_name}
    echo "#endif                                                " >> ${h_file_name}

    
    # Create the CMakeLists.txt file
    cd ../../
    rm ${cmake_file_name}
    echo "cmake_minimum_required(VERSION 2.8.3)                                            " >> ${cmake_file_name}
    echo "project(${package_name})                                                         " >> ${cmake_file_name}
    echo "                                                                                 " >> ${cmake_file_name}
    echo "## Find catkin and any catkin packages                                           " >> ${cmake_file_name}
    echo "find_package(catkin REQUIRED COMPONENTS ${@})                                    " >> ${cmake_file_name}
    echo "                                                                                 " >> ${cmake_file_name}
    echo "catkin_package(                                                                  " >> ${cmake_file_name}
    echo "    INCLUDE_DIRS include                                                         " >> ${cmake_file_name}
    echo "    CATKIN_DEPENDS ${@}                                                          " >> ${cmake_file_name}
    echo ")                                                                                " >> ${cmake_file_name}
    echo "                                                                                 " >> ${cmake_file_name}
    echo "include_directories(include \${catkin_INCLUDE_DIRS} include/\${PROJECT_NAME})    " >> ${cmake_file_name}
    echo "                                                                                 " >> ${cmake_file_name}
    echo "file(GLOB SOURCES \"src/*.cpp\")                                                 " >> ${cmake_file_name}
    echo "                                                                                 " >> ${cmake_file_name}
    echo "add_executable(${package_name} \${SOURCES})                                      " >> ${cmake_file_name}
    echo "                                                                                 " >> ${cmake_file_name}
    echo "add_dependencies( ${package_name}                                                " >> ${cmake_file_name}
    echo "    \${\${PROJECT_NAME}_EXPORTED_TARGETS}                                        " >> ${cmake_file_name}
    echo "    \${catkin_EXPORTED_TARGETS}                                                  " >> ${cmake_file_name}
    echo ")                                                                                " >> ${cmake_file_name}
    echo "                                                                                 " >> ${cmake_file_name}
    echo "                                                                                 " >> ${cmake_file_name}
    echo "target_link_libraries(                                                           " >> ${cmake_file_name}
    echo "    ${package_name}                                                              " >> ${cmake_file_name}
    echo "    \${catkin_LIBRARIES}                                                         " >> ${cmake_file_name}
    echo ")                                                                                " >> ${cmake_file_name}
    
    # Create the launch file
    cd ./launch
    launch_file_name="${package_name}.launch"
    echo "<launch>                                                                                                       " >> ${launch_file_name}
    echo "    <node pkg=\"${package_name}\" name=\"${package_name}\" type=\"${package_name}\" output=\"screen\"/>       " >> ${launch_file_name}
    echo "</launch>                                                                                                      " >> ${launch_file_name}
}


function configure_for_python () {
    echo -e "${TITLE_COLOR}Configuring as Python package${NC}"
    
    # Create the main src file
    cd src
    src_file_name="${package_name}.py"
    
    echo "#!/usr/bin/env python                                              " >> ${src_file_name}
    echo "                                                                   " >> ${src_file_name}
    echo "import rospy                                                       " >> ${src_file_name}
    echo "                                                                   " >> ${src_file_name}
    echo "class ${class_name}:                                               " >> ${src_file_name}
    echo "    def __init__(self):                                            " >> ${src_file_name}
    echo "        rospy.loginfo(\"Initializing ${class_name} object\")       " >> ${src_file_name}
    echo "                                                                   " >> ${src_file_name}
    echo "if __name__ == '__main__':                                         " >> ${src_file_name}
    echo "                                                                   " >> ${src_file_name}
    echo "    rospy.init_node('${package_name}')                             " >> ${src_file_name}
    echo "    rate = rospy.Rate(${default_hz}) #Hz                           " >> ${src_file_name}
    echo "                                                                   " >> ${src_file_name}
    echo "    ${package_name} = ${class_name}()                              " >> ${src_file_name}
    echo "                                                                   " >> ${src_file_name}
    echo "    try:                                                           " >> ${src_file_name}
    echo "        while not rospy.is_shutdown():                             " >> ${src_file_name}
    echo "            rate.sleep()                                           " >> ${src_file_name}
    echo "    except rospy.ROSInterruptException:                            " >> ${src_file_name}
    echo "        pass                                                       " >> ${src_file_name}
    
    # Set permissions on the file
    chmod 755 "${src_file_name}"
    
    # Create the launch file
    cd ../launch
    launch_file_name="${package_name}.launch"
    echo "<launch>                                                                                                       " >> ${launch_file_name}
    echo "    <node pkg=\"${package_name}\" name=\"${package_name}\" type=\"${src_file_name}\" output=\"screen\"/>       " >> ${launch_file_name}
    echo "</launch>                                                                                                      " >> ${launch_file_name}
}


# Create the package
if [ ! -d ~/catkin_ws/src/ ]
then
    # Create a catkin workspace if it does not exist
    source ${CREATE_SCRIPTS_DIR}/create_catkin_ws.sh
fi
cd ~/catkin_ws/src/

package_name=${1}
echo -e "${TITLE_COLOR}Creating ROS package: ${package_name}${NC}"
catkin_create_pkg $@
echo -e "${FINISHED_TITLE_COLOR}Finished creating ${package_name}${NC}"


# Figure out the class name
search_str="_"

# Get the index of search_str in package_name
index=`expr index "${package_name}" "${search_str}"`

# Initialize some variables for later
class_name=""
remaining_str="${package_name}"

if [ ${index} -eq 0 ]
then
    # Convert the first letter of first_half_str into a capital letter and then set it to class_name
    class_name="${package_name^}"
fi

# Keep looping while we still are able to find search_str in package_name
while [ ${index} -ne 0 ]
do
    # Get the first half of the string (between the first letter of remaining_str and index-1)
    first_half_str=${remaining_str:0:index-1}
    
    # Get the second half of the string (between the index and the end of the remaining_str)
    remaining_str=${remaining_str:index}
    
    # Convert the first letter of first_half_str into a capital letter and then add it to class_name
    class_name="${class_name}${first_half_str^}"
    
    # Get the index of search_str in remaining_str
    index=`expr index "${remaining_str}" "${search_str}"`
    
    # If we were not able to find the search_str...
    if [ "$index" -eq "0" ]
    then
        # Convert the first letter of remaining_str into a capital letter and then add it to class_name
        class_name="${class_name}${remaining_str^}"
    fi
done


# Configure the package
cd "${package_name}"
mkdir launch
# src directory is already created

# Loop through the command line args searching for specific ros package dependencies
while [ "${1}" != "" ]
do
     case ${1} in
        roscpp )
            configure_for_cpp ${@}
            ;;
        rospy )
            configure_for_python
            ;;
    esac
    shift
done

echo -e "${FINISHED_TITLE_COLOR}Finished configuring package${NC}"


# Compile the package so that when we source things it will be included
echo -e "${TITLE_COLOR}Compiling ${package_name}${NC}"
source ${CREATE_SCRIPTS_DIR}/catkin_build.sh
echo -e "${FINISHED_TITLE_COLOR}Finished compiling${NC}"


# Return to where we came from
cd ${SCRIPTS_DIR}
