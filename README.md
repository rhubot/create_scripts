# Description
The scripts within this repo aid in the creation and maintenance of common file, folder, and applications during the development stage of a project.

Note: `configure_create_scripts.sh` will set an environment variable via the `.bashrc` file. This allows the user to clone create_scripts to any location and allows other scripts to utilize the various scripts within create_scripts. The `configure_create_scripts.sh` script looks at the current directory when setting this variable, so it is required that the user is in the base of the create_scripts directory when that script is called.


# Setup
Install git
```
sudo apt-get install git
```
Get the scripts
```
git clone https://bitbucket.org/rhubot/create_scripts.git ~/create_scripts
```
Configure the environment
```
cd ~/create_scripts
./configure_create_scripts.sh
source ~/.bashrc
```


# Contents


## catkin_build.sh
Builds the code under the `~/catkin_ws/` directory. This scripts checks to see if Catkin Tools and utilizes it if available (i.e. the script decides to use `catkin_make` or `catkin build`).

**Usage**:
```
user@comp:~/create_scripts$ ./catkin_build.sh 
```


## catkin_build_clean.sh
Performs a "build clean" using the Catkin build system by deleting the generated directories (`devel`, `build`, and `logs`) under the `~/catkin_ws/` directory before performing a build command.

**Usage**:
```
user@comp:~/create_scripts$ ./catkin_build_clean.sh 
```


## catkin_set_active_ws.sh
This script will set an environment variable named `CATKIN_ACTIVE_DIR` via the `.bashrc` file that holds the location of active catkin workspace. This environment variable can then be used by other scripts to know the location of the active catkin workspace.

**Usage**:
```
user@comp:~/create_scripts$ ./catkin_set_active_ws.sh <path to active workspace>
```
where `path to active workspace` is the path to the local directory that contains the root of the active workspace (e.g. `~/catkin_ws`). If no path is specified, the value defaults to `~/catkin_ws`.


## configure_create_scripts.sh
This script will set an environment variable named `CREATE_SCRIPTS_DIR` via the `.bashrc` file that holds the location of the create_scripts. This variable allows other scripts to utilize the various scripts within create_scripts. The `configure_create_scripts.sh` script looks at the current directory when setting this variable, so it is required that the user is in the base of the create_scripts directory when that script is called.

**Usage**:
```
user@comp:~/create_scripts$ ./configure_create_scripts.sh
```


## create_catkin_ws.sh
Creates a Catkin workspace in the default location (`~/catkin_ws`), including any required sub-folders.

**Usage**:
```
user@comp:~/create_scripts$ ./create_catkin_ws.sh 
```


## create_git_branch.sh
Creates a new branch in the given local git repo and pushes it up to the remote

**Usage**:
```
user@comp:~/create_scripts$ ./create_git_branch.sh [options]
```

**Options**:
```
    --help, -h
        Show this help message
    --repo <path to local repo>, -r <path to local repo>
        Specify the local git repo
        Where <path to local repo> is the path to the folder containing the root of an existing git repo
    --branch <new branch name>, -b <new branch name>
        Specify the new branch name
        Where <new branch name> is the name of the new branch
```

**Example**:
```
user@comp:~/create_scripts$ ./create_git_branch.sh -r ~/path/to/repo -b new_branch_name"
```


## create_gym_env.sh
Creates the base code and helper scripts for an OpenAI Gym custom environment. This code is created within the `~/catkin_ws/src` directory.

**Usage**:
```
user@comp:~/create_scripts$ ./create_gym_env.sh <environment name>
```
Where `environment name` is the name of the environment, using the naming scheme of all lower-case letters and _ between words. E.g. `tic_tac_toe`


## create_gym_env_in_repo.sh
Calls `create_gym_env.sh` and then `make_into_git_repo.sh`

**Usage**:
```
user@comp:~/create_scripts$ ./create_gym_env_in_repo.sh <environment name>
```
See description of `create_gym_env.sh` and `make_into_git_repo.sh` for more details.


## create_menu_script.sh
Using a given configuration file, this script creates a new script containing a text-based menu. Based on what that user selects within the menu, the corresponding scripts will be executed. Existing configuration files are located in the `~/create_scripts/menu_script_configs/` folder and can be used as examples

**Usage**:
```
user@comp:~/create_scripts$ ./create_menu_script.sh <path to config file> <output script file name>
```
Where `path to config file` contains the path to the configuration file (see below for description of config file contents)

Where `output script file name` is the path and file name of the script to be created. Is is expected that this file is a bash script (i.e. the file name ends with `.sh`)


The config file adheres to the yaml format:
```
menu_group_name:
    title: Title to be displayed by menu
    script: the script to be executed (including file path) if chosen in the menu
    defaultChoice: Sets whether the menu item defaults to install (given a value of true) or not (false)
    giveSourceControlInfo: This item is becoming obsolete. This item lets the main script know if source control information (username and email) should be passed to the script described in this part of the config file
```
The following example will offer a menu item with the text `Kate (with configuration)`. if chosen by the user, the menu script will run the `install_kate.sh` script. It defaults to `true` with respect to running the scripts, and the menu system does not pass along source control information.
```
kate:
    title: Kate (with configuration)
    script: ${INSTALL_SCRIPTS_DIR}/install_kate.sh
    defaultChoice: true
    giveSourceControlInfo: false
```


## create_ros_pkg.sh
Creates a ROS package in the default catkin workspace (`~/catkin_ws/src/`). The script looks at the given ROS dependencies and determines if it is a C++ style package or a Python style package. Based on the style of the package, the script creates the appropriate source files, launch file, and further configures the package.

**Usage**:
```
user@comp:~/create_scripts$ ./create_ros_pkg.sh <package_name> [ros dependencies]
```
Where `package_name` is the name of the package to be created

Where `ros dependencies` is a list of ROS dependencies for the newly created package. These packages should be seperated by spaces.


## create_ros_pkg_in_repo.sh
Calls `create_ros_pkg.sh` and then `make_into_git_repo.sh`

**Usage**:
```
user@comp:~/create_scripts$ ./create_ros_pkg_in_repo.sh <package_name> [ros dependencies]
```
See description of `create_ros_pkg.sh` and `make_into_git_repo.sh` for more details.


## git_pull.sh
Performs a `git pull` command on several directories. The script searches sub-directories contained within the given directory (1 sub-directory deep, it does not search sub-directories of sub-directories). If a sub-directory is a GIT repo, it performs the `git pull` command. If no directory is specified when running this script, the script will use the user's home diretcory.

**Usage**:
```
user@comp:~/create_scripts$ ./git_pull.sh <directory to search>
```
Where `directory to search` is the directory containing one or more sub-directories that are GIT repos. This argument is optional and defaults to `~/` if not specified.


## git_status.sh
Performs a `git status` command on several directories. The script searches sub-directories contained within the given directory (1 sub-directory deep, it does not search sub-directories of sub-directories). If a sub-directory is a GIT repo, it performs the `git status` command. If no directory is specified when running this script, the script will use the user's home diretcory.

**Usage**:
```
user@comp:~/create_scripts$ ./git_status.sh <directory to search>
```
Where `directory to search` is the directory containing one or more sub-directories that are GIT repos. This argument is optional and defaults to `~/` if not specified.


## make_into_git_repo.sh
This script turns the given directory into both a local and remote GIT repo. This includes creating the remote GIT repo using Bitbucket's API (you must already have a Bitbucket account setup). The script creates a blank `README.md` if one does not exist. The script also keeps all existing files intact, adding the files to the repo, commiting them, and performing a push.

**Usage**:
```
user@comp:~/create_scripts$ ./make_into_git_repo.sh <path to local folder>
```
Where `path to local folder` is the path to the local folder that you wish to make into a GIT repo.

Note: the script will prompt the user for their username and password to help facilitate the process


## recreate_menu_scripts.sh
This script recreates all of the menus that were created using the `create_menu_script.sh` script. This script helps push changes to all the auto-generated menu scripts when a change has been made to the `create_menu_script.sh` script.

**Usage**:
```
user@comp:~/create_scripts$ ./recreate_menu_scripts.sh 
```

