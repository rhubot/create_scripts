#!/bin/bash -i

# This script performs the same as:
#   docker rmi $(docker images -a -q)

source ${INSTALL_SCRIPTS_DIR}/colors.sh

IMAGES=$(docker images -a -q)
if [[ ! ${IMAGES[0]} == "" ]]
then
    echo -e "${TITLE_COLOR}Removing images:${NC}\\n${IMAGES}"
    docker rmi --force ${IMAGES}
    echo -e " ${CHECK_MARK} ${FINISHED_TITLE_COLOR}Done!${NC}"
else
    echo -e " ${CHECK_MARK} ${WARNING_COLOR}No images to remove${NC}"
fi
