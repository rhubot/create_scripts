#!/bin/bash -i


# Setup error handling
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR


# Remember where we started
SCRIPTS_DIR=$(pwd)


# Get color definitions and other scripts
if [ ! -d ${INSTALL_SCRIPTS_DIR}/ ]
then
    git clone https://bitbucket.org/rhubot/install_scripts.git ~/install_scripts
    cd ~/install_scripts
    source ./configure_install_scripts.sh
    cd ${SCRIPTS_DIR}
    
    set +e
    trap - ERR
    source ~/.bashrc
    set -e
    trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR
    
fi
source ${INSTALL_SCRIPTS_DIR}/colors.sh ""


# A function that displays a help / usage message to the user
function displayHelp() {
    echo -e "Description:"
    echo -e "    Creates a new branch in the given local git repo and pushes it up to the remote"
    echo -e ""
    echo -e "Usage:"
    echo -e "    create_git_branch.sh [options]"
    echo -e ""
    echo -e "Options:"
    echo -e "    --help, -h"
    echo -e "        Show this help message"
    echo -e "    --repo <path to local repo>, -r <path to local repo>"
    echo -e "        Specify the local git repo"
    echo -e "        Where <path to local repo> is the path to the folder containing the root of an existing git repo"
    echo -e "    --branch <new branch name>, -b <new branch name>"
    echo -e "        Specify the new branch name"
    echo -e "        Where <new branch name> is the name of the new branch"
    echo -e ""
    echo -e "Example:"
    echo -e "    create_git_branch.sh -r ~/path/to/repo -b new_branch_name"
    
    # Exit out of this script
    exit
}


# Variables for command line args
repoPath=~/
branchName="new_branch"


# Process any command line args
while [ "${1}" != "" ]
do
    case ${1} in
        -h | --help )
            displayHelp
            ;;
        -r | --repo )
            shift
            repoPath=${1}
            ;;
        -b | --branch )
            shift
            branchName=${1}
            ;;
        * )
            echo -e "${WARNING_COLOR}Unknown option provided: ${1}${NC}"
            displayHelp
            ;;
    esac
    shift
done


# If we got the proper input, create the branch
if [ "${repoPath}" != "~/" ] && [ "${branchName}" != "new_branch" ]
then
    echo -e "${TITLE_COLOR}Creating branch...${NC}"
    
    cd ${repoPath}
    git checkout -b ${branchName}
    git push -u origin ${branchName}
    
    echo -e "${FINISHED_TITLE_COLOR}Finished creating the branch${NC}"
else
    displayHelp
fi


cd ${SCRIPTS_DIR}
