#!/bin/bash -i

active_dir=~/catkin_ws

if [ ${#} -eq 1 ]
then
    active_dir=${1}
fi

echo "export CATKIN_ACTIVE_DIR=${active_dir}" >> ~/.bashrc
set +e
source ~/.bashrc
