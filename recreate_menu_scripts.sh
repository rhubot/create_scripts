#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR


if [ -d ${INSTALL_SCRIPTS_DIR} ]
then
    ${CREATE_SCRIPTS_DIR}/create_menu_script.sh ${CREATE_SCRIPTS_DIR}/menu_script_configs/dev_apps.yaml ${INSTALL_SCRIPTS_DIR}/install_dev_apps.sh
    ${CREATE_SCRIPTS_DIR}/create_menu_script.sh ${CREATE_SCRIPTS_DIR}/menu_script_configs/python2.yaml ${INSTALL_SCRIPTS_DIR}/install_python_2_packages.sh
    ${CREATE_SCRIPTS_DIR}/create_menu_script.sh ${CREATE_SCRIPTS_DIR}/menu_script_configs/python3.yaml ${INSTALL_SCRIPTS_DIR}/install_python_3_packages.sh
fi

if [ -d ${SETUP_SCRIPTS_DIR} ]
then
    ${CREATE_SCRIPTS_DIR}/create_menu_script.sh ${CREATE_SCRIPTS_DIR}/menu_script_configs/ml_sandbox.yaml ${SETUP_SCRIPTS_DIR}/setup_ml_sandbox_menu.sh
fi

set +e
