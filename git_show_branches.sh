#!/bin/bash -i

# Get color definitions and other scripts
if [ ! -d ${INSTALL_SCRIPTS_DIR}/ ]
then
    git clone https://bitbucket.org/rhubot/install_scripts.git ~/install_scripts
    source ~/install_scripts/configure_install_scripts.sh
    
    set +e
    trap - ERR
    source ~/.bashrc
    set -e
    trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR
    
fi
source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

# Remember where we started
SCRIPTS_DIR=$(pwd)

false=0
true=1

givenDirIsRepo=${true}

# Default search directory
SEARCH_DIR=".."

# Get the Git branch of a given directory and display it
function display_branches () {
    
    git_branch_all=$(git status -b --porcelain 2>&1)
    CURRENT_DIR=$(pwd)
    if [[ ${git_branch_all} == *"not a git repository"* ]] || [[ ${git_branch_all} == *"Not a git repository"* ]]
    then
        givenDirIsRepo=${false}
    else
        # Remove "##" from front
        git_branch_temp=${git_branch_all:2:${#git_branch_all}}
        
        # Change our Internal Field Seperator and grab everything before "."
        ORIG_IFS=${IFS}
        IFS='.'
        set -- ${git_branch_temp}
        git_branch=${1}
        IFS=${ORIG_IFS}
        
        echo -e "${LIGHT_PURPLE}${CURRENT_DIR}${NC} --> ${AQUA}${git_branch}${NC}"
    fi
}

# If we are given a directory to search, use that directory instead
if test $# -eq 1
then
    if [ "${1}" == "--ros" ]
    then
        SEARCH_DIR="../catkin_ws/src/"
    else
        SEARCH_DIR=${1}
    fi
    
    # Do the search
    cd ${SEARCH_DIR}
    echo ""
    display_branches ${SEARCH_DIR}
    
    # ... and sub-directories (1 level deep) if the given directory was not a repo
    if [ ${givenDirIsRepo} -eq ${false} ]
    then
        for d in ./*/
        do
            # if the directory ${d} exists...
            if [ -d ${d} ]
            then
                cd ${d}
                display_branches ${d}
                cd ..
            fi
        done
    fi
    echo ""
elif test $# -eq 0
then
    source ./git_show_branches.sh ~
    
    if [[ -d ~/catkin_ws/src/ ]]
    then
        source ./git_show_branches.sh --ros
    fi
fi


# Return to the original directory
cd ${SCRIPTS_DIR}
