#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

# Get color definitions and other scripts
if [ ! -d ${INSTALL_SCRIPTS_DIR}/ ]
then
    git clone https://bitbucket.org/rhubot/install_scripts.git ~/install_scripts
    source ~/install_scripts/configure_install_scripts.sh
    
    set +e
    trap - ERR
    source ~/.bashrc
    set -e
    trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR
    
fi
source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

# Make sure we are given the proper number of command line arguments
if test $# -ne 2
then
    echo -e "${ERROR_COLOR}Usage: ${0} [path to config file] [output script file]${NC}"
    echo -e "${ERROR_COLOR}Given $# command line arguments${NC}"
    exit 1
fi

# Get the command line arguments
config_file=${1}
generated_script_file=${2}

# Make variables for holding info
declare -a menu_options
declare -a script_files
declare -a default_install_choice
declare -a pass_source_control_info
index=0
delimiter=";"






false=0
true=1

foundStart=${false}
foundEnd=${false}

menuIndex=-1

# Read the config file
while read line
do
    
    if [ "${line}" == "---" ]
    then
        foundStart=${true}
    elif [ "${line}" == "..." ]
    then
        foundEnd=${true}
    elif [ ${foundStart} -eq ${true} ] && [ ${foundEnd} -eq ${false} ]
    then


        env_name=${line}             # e.g. tic_tac_toe
        common_name=""               # e.g. Tic Tac Toe


        # Figure out the common name
        search_str=":"

        # Get the index of search_str in env_name
        index=`expr index "${env_name}" "${search_str}"`

        # Initialize some variables for later
        common_name=""
        remaining_str="${env_name}"

        # Keep looping while we still are able to find search_str in env_name
        while [ ${index} -ne 0 ]
        do
            # Get the first half of the string (between the first letter of remaining_str and index-1)
            first_half_str=${remaining_str:0:index-1}
            
            # Get the second half of the string (between the index and the end of the remaining_str)
            remaining_str=${remaining_str:index}
            # Remove leading whitespace
            remaining_str="$(echo -e "${remaining_str}" | sed -e 's/^[[:space:]]*//')"
            
            
            if [ "${first_half_str}" != "" ] && [ "${remaining_str}" == "" ]
            then
                menuIndex=$((menuIndex + 1))
                #echo "Menu index: ${menuIndex}"
            elif [ "${first_half_str}" == "title" ]
            then
                #echo "Setting menu title: ${remaining_str}"
                menu_options[${menuIndex}]=${remaining_str}
            elif [ "${first_half_str}" == "script" ]
            then
                #echo "Setting script: ${remaining_str}"
                script_files[${menuIndex}]=${remaining_str}
            elif [ "${first_half_str}" == "defaultChoice" ]
            then
                #echo "Setting default install: ${remaining_str}"
                default_install_choice[${menuIndex}]=${remaining_str}
            elif [ "${first_half_str}" == "giveSourceControlInfo" ]
            then
                #echo "Setting pass to sc: ${remaining_str}"
                pass_source_control_info[${menuIndex}]=${remaining_str}
            fi
            
            # Get the index of search_str in remaining_str
            if [ "${remaining_str}" != "" ]
            then
                set +e
                trap - ERR
                index=`expr index "${remaining_str}" "${search_str}"`
                set -e
                trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR
            else
                index=0
            fi
            
        done
        
        
        #echo "------"
    fi

done < ${config_file}
#while read line
#do
#    # For each line of the config file, split it into a script file and menu option
#    temp_script_file=$(echo ${line}| cut -d ${delimiter} -f 1)
#    temp_default_install_choice=$(echo ${line}| cut -d ${delimiter} -f 2)
#    temp_pass_source_control_info=$(echo ${line}| cut -d ${delimiter} -f 3)
#    temp_menu_option=$(echo ${line}| cut -d ${delimiter} -f 4-100)
#    
#    # Ignore lines that start with #
#    if [[ "${temp_script_file}" != "#"* ]]
#    then
#        # Store those values to the proper arrays
#        script_files[${index}]=${temp_script_file}
#        default_install_choice[${index}]=${temp_default_install_choice}
#        pass_source_control_info[${index}]=${temp_pass_source_control_info}
#        menu_options[${index}]=${temp_menu_option}
#    fi
#    
#    # Increment the index
#    index=$((index+1))
#done < ${config_file}

if test -f "${generated_script_file}"
then
    echo -e "${WARNING_COLOR}Removing old script file: ${generated_script_file}${NC}"
    rm ${generated_script_file}
fi

numMenuOptions=${#menu_options[@]}

echo -e "${TITLE_COLOR}Creating ${generated_script_file}${NC}"

# Create the output script
echo "#!/bin/bash -i                                                                                                                          " >> ${generated_script_file}
echo "# NOTE: This script was auto generated                                                                                                  " >> ${generated_script_file}
echo "                                                                                                                                        " >> ${generated_script_file}
echo "set -e                                                                                                                                  " >> ${generated_script_file}
echo "trap 'source \${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh \${BASH_LINENO} \${0} \${LINENO} \${BASH_ARGC} \${BASH_ARGV} \${BASH_COMMAND}' ERR " >> ${generated_script_file}
echo "                                                                                                                                        " >> ${generated_script_file}
echo "source \${INSTALL_SCRIPTS_DIR}/colors.sh \"\"                                                                                                 " >> ${generated_script_file}
echo "                                                                                                                                        " >> ${generated_script_file}
echo "# Specify yes/no values                                                                                                                 " >> ${generated_script_file}
echo "affirm=\"y\"                                                                                                                            " >> ${generated_script_file}
echo "neg=\"n\"                                                                                                                               " >> ${generated_script_file}
echo "unknown=\"maybe\"                                                                                                                       " >> ${generated_script_file}
echo "                                                                                                                                        " >> ${generated_script_file}
echo "sc_user=\"\"                                                                                                                            " >> ${generated_script_file}
echo "sc_email=\"\"                                                                                                                           " >> ${generated_script_file}
echo "                                                                                                                                        " >> ${generated_script_file}
echo "# Set default values                                                                                                                    " >> ${generated_script_file}
                                                                                                         
index=0                                                                                                  
for i in "${default_install_choice[@]}"                                                                  
do                                                                                                       
    if [ ${i} == "true" ]                                                                                
    then                                                                                                 
        echo "choice_${index}=\$affirm                                                                   " >> ${generated_script_file}
    else                                                                                                 
        echo "choice_${index}=\$neg                                                                      " >> ${generated_script_file}
    fi                                                                                                   
                                                                                                         
    # Increment the index                                                                                
    index=$((index+1))                                                                                   
done                                                                                                     
                                                                                                         
echo "choice_done=\$unknown                                                                              " >> ${generated_script_file}
echo "                                                                                                   " >> ${generated_script_file}
echo "# Set index values                                                                                 " >> ${generated_script_file}
echo "index_done=0                                                                                       " >> ${generated_script_file}
                                                                                                         
index=0                                                                                                  
for i in "${menu_options[@]}"                                                                            
do                                                                                                       
    echo "index_${index}=$((index+1))                                                                    " >> ${generated_script_file}
    
    # Increment the index
    index=$((index+1))
done

echo "                                                                                                                                 " >> ${generated_script_file}
echo "# Menu variables                                                                                                                 " >> ${generated_script_file}
echo "currentMenuIndex=1                                                                                                               " >> ${generated_script_file}
echo "user_choice=\$affirm                                                                                                             " >> ${generated_script_file}
echo "default_choice=\$unkonwn                                                                                                         " >> ${generated_script_file}
echo "                                                                                                                                 " >> ${generated_script_file}
echo "# Display the menu                                                                                                               " >> ${generated_script_file}
echo "fullMenu()                                                                                                                       " >> ${generated_script_file}
echo "{                                                                                                                                " >> ${generated_script_file}
echo "    clear                                                                                                                        " >> ${generated_script_file}
echo "    echo -e \"\${WARNING_COLOR}NOTE: If you are looking to setup an entire project, you may prefer to use setup_scripts\${NC}\"  " >> ${generated_script_file}
echo "    echo \"\"                                                                                                                    " >> ${generated_script_file}
echo "    echo \"\"                                                                                                                    " >> ${generated_script_file}
echo "    echo \"Which apps would you like to install?\"                                                                               " >> ${generated_script_file}
echo "    echo \"\"                                                                                                                    " >> ${generated_script_file}
echo "                                                                                                                                 " >> ${generated_script_file}

index=0
for i in "${menu_options[@]}"
do
    echo "    # ${i}                                                                                     " >> ${generated_script_file}
    echo "    if [ \$currentMenuIndex -eq \$index_${index} ]                                             " >> ${generated_script_file}
    echo "    then                                                                                       " >> ${generated_script_file}
    echo "        default_choice=\$choice_${index}                                                       " >> ${generated_script_file}
    echo "        echo -e \"\${PROMPT_COLOR}--> [\"\$choice_${index}\"] ${i}\${NC}\"                     " >> ${generated_script_file}
    echo "    elif [ \"\$choice_${index}\" == \"\$affirm\" ]                                             " >> ${generated_script_file}
    echo "    then                                                                                       " >> ${generated_script_file}
    echo "        echo -e \"\${LIME}    [\"\$choice_${index}\"] ${i}\${NC}\"                             " >> ${generated_script_file}
    echo "    else                                                                                       " >> ${generated_script_file}
    echo "        echo -e \"\${MAROON}    [\"\$choice_${index}\"] ${i}\${NC}\"                           " >> ${generated_script_file}
    echo "    fi                                                                                         " >> ${generated_script_file}
    echo "                                                                                               " >> ${generated_script_file}
    
    # Increment the index
    index=$((index+1))
done

echo "    # Done                                                                                         " >> ${generated_script_file}
echo "    if [ \$currentMenuIndex -eq \$index_done ]                                                     " >> ${generated_script_file}
echo "    then                                                                                           " >> ${generated_script_file}
echo "        default_choice=\$affirm                                                                    " >> ${generated_script_file}
echo "        echo -e \"\${PROMPT_COLOR}--> [y] Done\${NC}\"                                             " >> ${generated_script_file}
echo "    else                                                                                           " >> ${generated_script_file}
echo "        echo \"    [ ] Done\"                                                                      " >> ${generated_script_file}
echo "    fi                                                                                             " >> ${generated_script_file}
echo "                                                                                                   " >> ${generated_script_file}
echo "    echo \"\"                                                                                      " >> ${generated_script_file}
echo "    echo \"Menu options:\"                                                                         " >> ${generated_script_file}
echo "    echo \"  'y' or 'n' to choose if you want to install the currently selected app\"              " >> ${generated_script_file}
echo "    echo \"  [enter] to accept current value\"                                                     " >> ${generated_script_file}
echo "    echo \"  'a' to install all listed apps\"                                                      " >> ${generated_script_file}
echo "    echo \"  'd' to install the currently selected apps\"                                          " >> ${generated_script_file}
echo "    echo \"  'q' to quit without installing anything\"                                             " >> ${generated_script_file}
echo "    echo \"  'p' to move to the previously selected app\"                                          " >> ${generated_script_file}
echo "                                                                                                   " >> ${generated_script_file}
echo "    # Read the user's choice                                                                       " >> ${generated_script_file}
echo "    read user_choice                                                                               " >> ${generated_script_file}
echo "                                                                                                   " >> ${generated_script_file}
echo "    # Process the user's choice                                                                    " >> ${generated_script_file}
echo "    if [ \$user_choice == \"a\" ]                                                                  " >> ${generated_script_file}
echo "    then                                                                                           " >> ${generated_script_file}
echo "        echo -e \"\${TITLE_COLOR}Installing all apps\${NC}\"                                       " >> ${generated_script_file}

index=0
for i in "${default_install_choice[@]}"
do
    echo "        choice_${index}=\$affirm    " >> ${generated_script_file}
    
    # Increment the index
    index=$((index+1))
done

echo "        choice_done=\$affirm                                                                       " >> ${generated_script_file}
echo "    elif [ \$user_choice == \"d\" ]                                                                " >> ${generated_script_file}
echo "    then                                                                                           " >> ${generated_script_file}
echo "        echo -e \"\${TITLE_COLOR}Installing currently selected apps\${NC}\"                        " >> ${generated_script_file}
echo "        choice_done=\$affirm                                                                       " >> ${generated_script_file}
echo "    elif [ \$user_choice == \"q\" ]                                                                " >> ${generated_script_file}
echo "    then                                                                                           " >> ${generated_script_file}
echo "        echo -e \"\${WARNING_COLOR}Exiting without installing anything\${NC}\"                     " >> ${generated_script_file}

index=0
for i in "${default_install_choice[@]}"
do
    echo "        choice_${index}=\$neg    " >> ${generated_script_file}
    
    # Increment the index
    index=$((index+1))
done

echo "        choice_done=\$affirm                                                                       " >> ${generated_script_file}
echo "    elif [ \$user_choice == \"p\" ]                                                                " >> ${generated_script_file}
echo "    then                                                                                           " >> ${generated_script_file}
echo "        user_choice=\$default_choice                                                               " >> ${generated_script_file}
echo "        decrementMenuIndex                                                                         " >> ${generated_script_file}
echo "    elif [ \$user_choice == \"y\" ]                                                                " >> ${generated_script_file}
echo "    then                                                                                           " >> ${generated_script_file}
echo "        updateChoice                                                                               " >> ${generated_script_file}
echo "    elif [ \$user_choice == \"n\" ]                                                                " >> ${generated_script_file}
echo "    then                                                                                           " >> ${generated_script_file}
echo "        updateChoice                                                                               " >> ${generated_script_file}
echo "    else                                                                                           " >> ${generated_script_file}
echo "        echo \"----->\"\$user_choice\"<-----\"                                                     " >> ${generated_script_file}
echo "        echo \"Using default value\"                                                               " >> ${generated_script_file}
echo "        user_choice=\$default_choice                                                               " >> ${generated_script_file}
echo "                                                                                                   " >> ${generated_script_file}
echo "        if [ \$currentMenuIndex -eq \$index_done ]                                                 " >> ${generated_script_file}
echo "        then                                                                                       " >> ${generated_script_file}
echo "            user_choice=\$affirm                                                                   " >> ${generated_script_file}
echo "        fi                                                                                         " >> ${generated_script_file}
echo "        updateChoice                                                                               " >> ${generated_script_file}
echo "    fi                                                                                             " >> ${generated_script_file}
echo "}                                                                                                  " >> ${generated_script_file}

echo "                                                                                                   " >> ${generated_script_file}
echo "# Update the choice variables based on user input                                                  " >> ${generated_script_file}
echo "updateChoice()                                                                                     " >> ${generated_script_file}
echo "{                                                                                                  " >> ${generated_script_file}
echo "    # Process user's y/n choice                                                                    " >> ${generated_script_file}
                                                                                                         
index=0                                                                                                  
echo "    if [ \$currentMenuIndex -eq \$index_${index} ]                                                 " >> ${generated_script_file}
echo "    then                                                                                           " >> ${generated_script_file}
echo "        choice_${index}=\$user_choice                                                              " >> ${generated_script_file}
echo "        incrementMenuIndex                                                                         " >> ${generated_script_file}
                                                                                                         
# Increment the index                                                                                    
index=$((index+1))                                                                                       
                                                                                                         
for (( i=0; i<$((numMenuOptions-1)); i++ ))                                                              
do                                                                                                       
    echo "    elif [ \$currentMenuIndex -eq \$index_${index} ]                                           " >> ${generated_script_file}
    echo "    then                                                                                       " >> ${generated_script_file}
    echo "        choice_${index}=\$user_choice                                                          " >> ${generated_script_file}
    echo "        incrementMenuIndex                                                                     " >> ${generated_script_file}
                                                                                                         
    # Increment the index                                                                                
    index=$((index+1))                                                                                   
done                                                                                                     
                                                                                                         
echo "    elif [ \$currentMenuIndex -eq \$index_done ]                                                   " >> ${generated_script_file}
echo "    then                                                                                           " >> ${generated_script_file}
echo "        choice_done=\$user_choice                                                                  " >> ${generated_script_file}
echo "    else                                                                                           " >> ${generated_script_file}
echo "        echo -e \"\${WARNING_COLOR}Menu index got messed up: \${NC}\" \$currentMenuIndex           " >> ${generated_script_file}
echo "        choice_done=\$affirm                                                                       " >> ${generated_script_file}
echo "    fi                                                                                             " >> ${generated_script_file}
echo "}                                                                                                  " >> ${generated_script_file}
echo "                                                                                                   " >> ${generated_script_file}
echo "# Decrement to the previous menu item                                                              " >> ${generated_script_file}
echo "decrementMenuIndex()                                                                               " >> ${generated_script_file}
echo "{                                                                                                  " >> ${generated_script_file}
                                                                                                         
index=0                                                                                                  
echo "    if [ \$currentMenuIndex -eq \$index_${index} ]                                                 " >> ${generated_script_file}
echo "    then                                                                                           " >> ${generated_script_file}
echo "        currentMenuIndex=\$index_${index}                                                          " >> ${generated_script_file}
                                                                                                         
# Increment the index                                                                                    
index=$((index+1))                                                                                       
                                                                                                         
for (( i=0; i<$((numMenuOptions-1)); i++ ))                                                              
do                                                                                                       
    echo "    elif [ \$currentMenuIndex -eq \$index_${index} ]                                           " >> ${generated_script_file}
    echo "    then                                                                                       " >> ${generated_script_file}
    echo "        currentMenuIndex=\$index_$((index-1))                                                  " >> ${generated_script_file}
                                                                                                         
    # Increment the index                                                                                
    index=$((index+1))                                                                                   
done                                                                                                     
                                                                                                         
echo "    elif [ \$currentMenuIndex -eq \$index_done ]                                                   " >> ${generated_script_file}
echo "    then                                                                                           " >> ${generated_script_file}
echo "        currentMenuIndex=\$index_$((index-1))                                                      " >> ${generated_script_file}
echo "    else                                                                                           " >> ${generated_script_file}
echo "        currentMenuIndex=\$index_done                                                              " >> ${generated_script_file}
echo "    fi                                                                                             " >> ${generated_script_file}
echo "}                                                                                                  " >> ${generated_script_file}

echo "                                                                                                   " >> ${generated_script_file}
echo "# Increment to the next menu item                                                                  " >> ${generated_script_file}
echo "incrementMenuIndex()                                                                               " >> ${generated_script_file}
echo "{                                                                                                  " >> ${generated_script_file}
                                                                                                         
index=0                                                                                                  
echo "    if [ \$currentMenuIndex -eq \$index_${index} ]                                                 " >> ${generated_script_file}
echo "    then                                                                                           " >> ${generated_script_file}
echo "        currentMenuIndex=\$index_$((index+1))                                                      " >> ${generated_script_file}
                                                                                                         
# Increment the index                                                                                    
index=$((index+1))                                                                                       
                                                                                                         
for (( i=0; i<$((numMenuOptions-2)); i++ ))                                                              
do                                                                                                       
                                                                                                         
    echo "    elif [ \$currentMenuIndex -eq \$index_${index} ]                                           " >> ${generated_script_file}
    echo "    then                                                                                       " >> ${generated_script_file}
    echo "        currentMenuIndex=\$index_$((index+1))                                                  " >> ${generated_script_file}
                                                                                                         
    # Increment the index                                                                                
    index=$((index+1))                                                                                   
                                                                                                         
done                                                                                                     
                                                                                                         
echo "    elif [ \$currentMenuIndex -eq \$index_${index} ]                                               " >> ${generated_script_file}
echo "    then                                                                                           " >> ${generated_script_file}
echo "        currentMenuIndex=\$index_done                                                              " >> ${generated_script_file}
echo "    else                                                                                           " >> ${generated_script_file}
echo "        currentMenuIndex=\$index_done                                                              " >> ${generated_script_file}
echo "    fi                                                                                             " >> ${generated_script_file}
echo "}                                                                                                  " >> ${generated_script_file}
                                                                                                         
echo "                                                                                                   " >> ${generated_script_file}
echo "# Loop thorugh the menu until done                                                                 " >> ${generated_script_file}
echo "loop()                                                                                             " >> ${generated_script_file}
echo "{                                                                                                  " >> ${generated_script_file}
echo "    fullMenu                                                                                       " >> ${generated_script_file}
echo "                                                                                                   " >> ${generated_script_file}
echo "    if [ \$choice_done == \$affirm ]                                                               " >> ${generated_script_file}
echo "    then                                                                                           " >> ${generated_script_file}
echo "        echo -e \"\${FINISHED_TITLE_COLOR}Done\${NC}\"                                             " >> ${generated_script_file}
echo "    elif [ \$choice_done == \$neg ]                                                                " >> ${generated_script_file}
echo "    then                                                                                           " >> ${generated_script_file}
echo "        currentMenuIndex=1                                                                         " >> ${generated_script_file}
echo "        choice_done=\$unknown                                                                      " >> ${generated_script_file}
echo "        loop                                                                                       " >> ${generated_script_file}
echo "    else                                                                                           " >> ${generated_script_file}
echo "        loop                                                                                       " >> ${generated_script_file}
echo "    fi                                                                                             " >> ${generated_script_file}
echo "}                                                                                                  " >> ${generated_script_file}

echo "                                                                                                  " >> ${generated_script_file}
echo "# When installing source control, we will need more info                                          " >> ${generated_script_file}
echo "getSourceControlParams()                                                                          " >> ${generated_script_file}
echo "{                                                                                                 " >> ${generated_script_file}
echo "    read -p \"\$(echo -e \${PROMPT_COLOR}\"Username for source control: \"\${NC})\" sc_user       " >> ${generated_script_file}
echo "    read -p \"\$(echo -e \${PROMPT_COLOR}\"Email for source control: \"\${NC})\" sc_email         " >> ${generated_script_file}
echo "}                                                                                                 " >> ${generated_script_file}
echo "                                                                                                  " >> ${generated_script_file}

echo "                                                                                                   " >> ${generated_script_file}
echo "                                                                                                   " >> ${generated_script_file}
echo "# Start the menu loop                                                                              " >> ${generated_script_file}
echo "loop                                                                                               " >> ${generated_script_file}
echo "                                                                                                   " >> ${generated_script_file}
echo "# Install stuff                                                                                    " >> ${generated_script_file}
echo "if [ \$user_choice != \"q\" ]                                                                      " >> ${generated_script_file}
echo "then                                                                                               " >> ${generated_script_file}
echo "    clear                                                                                          " >> ${generated_script_file}
echo "    getSourceControlParams                                                                         " >> ${generated_script_file}
echo "    clear                                                                                          " >> ${generated_script_file}
echo "    echo -e \"\"                                                                                   " >> ${generated_script_file}
echo "    echo -e \"\${TITLE_COLOR}~~~~~ Installing apps ~~~~~\${NC}\"                                   " >> ${generated_script_file}
echo "    echo -e \"\"                                                                                   " >> ${generated_script_file}
echo "    sudo apt-get update                                                                            " >> ${generated_script_file}
echo "                                                                                                   " >> ${generated_script_file}
                                                                                                         
echo "                                                                                                   " >> ${generated_script_file}
echo "    # Install the selected apps                                                                    " >> ${generated_script_file}
                                                                                                         
index=0                                                                                                  
for i in "${script_files[@]}"                                                                            
do                                                                                                       
    echo "                                                                                               " >> ${generated_script_file}
    echo "if [ \"\${choice_${index},,}\" == \"\${affirm,,}\" ]                                           " >> ${generated_script_file}
    echo "then                                                                                           " >> ${generated_script_file}
    if [ "${pass_source_control_info[${index}]}" == "true" ]
    then
        echo "    source ${i} --sc_user \${sc_user} --sc_email \${sc_email}                            " >> ${generated_script_file}
    else    
        echo "    source ${i}                                                                          " >> ${generated_script_file}
    fi
    echo "fi                                                                                             " >> ${generated_script_file}
                                                                                                         
    # Increment the index                                                                                
    index=$((index+1))                                                                                   
done                                                                                                     
                                                                                                         
echo "                                                                                                   " >> ${generated_script_file}
echo "    echo -e \"\"                                                                                   " >> ${generated_script_file}
echo "    echo -e \"\${FINISHED_TITLE_COLOR}***** Finished installing apps *****\${NC}\"                 " >> ${generated_script_file}
echo "    echo -e \"\"                                                                                   " >> ${generated_script_file}
echo "fi                                                                                                 " >> ${generated_script_file}
echo "                                                                                                   " >> ${generated_script_file}
echo "trap - ERR                                                                                         " >> ${generated_script_file}
#echo "                                                                                                   " >> ${generated_script_file}

# Make it executable
chmod 755 ${generated_script_file}

echo -e "${FINISHED_TITLE_COLOR}Finished creating ${generated_script_file}${NC}"

trap - ERR
