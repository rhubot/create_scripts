#!/bin/bash

if [[ ! -f ~/catkin_ws/src ]]
then
    mkdir -p ~/catkin_ws/src
fi

cd ~/catkin_ws/src
if [[ ! -f ~/catkin_ws/src/CMakeLists.txt ]]
then
    catkin_init_workspace
fi

if [[ ! -f ~/catkin_ws/src/.rosinstall ]]
then
    wstool init
fi

source ${CREATE_SCRIPTS_DIR}/catkin_build.sh
