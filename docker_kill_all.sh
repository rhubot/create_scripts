#!/bin/bash -i

# This script performs the same as:
#   docker kill $(docker ps -a -q)

source ${INSTALL_SCRIPTS_DIR}/colors.sh

CONTAINERS=$(docker ps -a -q)
if [[ ! ${CONTAINERS[0]} == "" ]]
then
    echo -e "${TITLE_COLOR}Killing containers:${NC}\\n${CONTAINERS}"
    docker kill ${CONTAINERS}
    echo -e " ${CHECK_MARK} ${FINISHED_TITLE_COLOR}Done!${NC}"
else
    echo -e " ${CHECK_MARK} ${WARNING_COLOR}No containers to kill${NC}"
fi
