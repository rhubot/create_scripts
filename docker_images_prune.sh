#!/bin/bash -i

source ${INSTALL_SCRIPTS_DIR}/colors.sh

echo -e "${TITLE_COLOR}Pruning images:${NC}"
docker image prune -f
echo -e " ${CHECK_MARK} ${FINISHED_TITLE_COLOR}Done!${NC}"
