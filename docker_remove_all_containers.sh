#!/bin/bash -i

# This script performs the same as:
#   docker rm $(docker ps -a -q)

source ${INSTALL_SCRIPTS_DIR}/colors.sh

source ${CREATE_SCRIPTS_DIR}/docker_kill_all.sh

CONTAINERS=$(docker ps -a -q)
if [[ ! ${CONTAINERS[0]} == "" ]]
then
    echo -e "${TITLE_COLOR}Removing containers:${NC}\\n${CONTAINERS}"
    docker rm --force ${CONTAINERS}
    echo -e " ${CHECK_MARK} ${FINISHED_TITLE_COLOR}Done!${NC}"
else
    echo -e " ${CHECK_MARK} ${WARNING_COLOR}No containers to remove${NC}"
fi
