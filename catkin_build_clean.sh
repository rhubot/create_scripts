#!/bin/bash -i

cd ~/catkin_ws

if [[ -d ./build/ ]]
then
    echo -n "Cleaning build files... "
    rm -rf ./build/
else
    echo -n "Searching for previous build artifacts... "
fi

if [[ -d ./devel/ ]]
then
    rm -rf ./devel/
fi

if [[ -d ./logs/ ]]
then
    rm -rf ./logs/
fi

echo "Done!"

source ${CREATE_SCRIPTS_DIR}/catkin_build.sh
